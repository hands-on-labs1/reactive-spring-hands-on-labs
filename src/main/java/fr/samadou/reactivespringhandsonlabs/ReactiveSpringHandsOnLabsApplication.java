package fr.samadou.reactivespringhandsonlabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveSpringHandsOnLabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveSpringHandsOnLabsApplication.class, args);
	}

}
